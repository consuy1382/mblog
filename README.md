### Mblog 开源Java博客系统, 支持多用户, 支持切换主题


### 技术选型：

* JDK8
* MySQL
* Spring-boot
* Spring-data-jpa
* Shiro
* Lombok
* Freemarker
* Bootstrap
* SeaJs

### 启动：
 - main方法运行
 ```xml
 配置：src/main/resources/application-mysql.yml (数据库账号密码)、新建db_mblog的数据库
 运行：src/main/java/com/mtons/mblog/BootApplication
 访问：http://localhost:8080/
 后台：http://localhost:8080/admin
 账号：默认管理员账号为 admin/12345
 
 TIPS: 
 如遇到启动失败/切换环境变量后启动失败的,请先maven clean后再尝试启动
 IDE得装lombok插件
```

    
### 图片演示 
![首页](https://images.gitee.com/uploads/images/2019/0316/141108_55f23b45_8684.png "前台首页.jpg")


### 和作者有哪些不同？
 * 增加了一套皮肤，皮肤名称：lofter
 * 增加了爬取数据（同花顺数据，新浪财经数据）
 * 增加了财富代码人气排行
 * 增加了财富昨天成交量明细以及占比
 * 增加了财富年度涨跌幅统计
 * 增加了节假日管理（菜单没放出来），api地址：/api/holiday/20190501
 * 增加了bnaner更换

### 里面还有哪些功能？
 * 里面有数据爬取规则
 * 数据爬取线程（900个线程），一般大A股只需要15分钟即可爬取完成（两年数据，大概80w条吧）

### 还计划增加哪些？
 * 增加本科、硕士、博士论文爬取（完成60%）
 * 增加财富代码全年涨跌幅排行榜（次功能只针对有需要的人开放，已经集成，体验版私密我）
 * 增加广告代码
 * 小程序关联（完成80%）
 * 公众号关联（完成80%）
 * 暂时就想到这些了，想到了再加

