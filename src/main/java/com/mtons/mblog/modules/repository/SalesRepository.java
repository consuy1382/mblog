/**  
* @Title: SalesRepository.java 
* @Description:
* @Copyright: Copyright (c) 2018
* @Company:http://www.sinocon.cn
* @author: Su.Yuanlin  
* @date 2019年4月11日  
* @QQ 314078331  
* @Like 流水落花春去也，天上人间
*/
package com.mtons.mblog.modules.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.mtons.mblog.modules.entity.Sales;

/**
 * @Title: SalesRepository
 * @Description:
 * @author:Su.Yuanlin
 * @date 2019年4月11日
 */
public interface SalesRepository extends JpaRepository<Sales, String>, JpaSpecificationExecutor<Sales> {

}
