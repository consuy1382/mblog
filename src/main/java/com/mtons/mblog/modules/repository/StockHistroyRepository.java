/**  
* @Title: StockHistroyRepository.java 
* @Description:
* @Copyright: Copyright (c) 2018
* @Company:http://www.sinocon.cn
* @author: Su.Yuanlin  
* @date 2019年3月14日  
* @QQ 314078331  
* @Like 流水落花春去也，天上人间
*/  
package com.mtons.mblog.modules.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import com.mtons.mblog.modules.entity.StockHistory;

/**  
* @Title: StockHistroyRepository 
* @Description:
* @author:Su.Yuanlin
* @date 2019年3月14日  
*/
public interface StockHistroyRepository  extends JpaRepository<StockHistory, String>, JpaSpecificationExecutor<StockHistory>{
	
}
