/**  
* @Title: HolidayRepository.java 
* @Description:
* @Copyright: Copyright (c) 2018
* @Company:http://www.sinocon.cn
* @author: Su.Yuanlin  
* @date 2019年3月18日  
* @QQ 314078331  
* @Like 流水落花春去也，天上人间
*/  
package com.mtons.mblog.modules.repository;

import java.util.Collection;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.mtons.mblog.modules.entity.Comment;
import com.mtons.mblog.modules.entity.Holiday;

/**  
* @Title: HolidayRepository 
* @Description:
* @author:Su.Yuanlin
* @date 2019年3月18日  
*/
public interface HolidayRepository extends JpaRepository<Holiday, String>, JpaSpecificationExecutor<Holiday> {
	List<Holiday> removeByDateCodeIn(Collection<String> ids);
}
