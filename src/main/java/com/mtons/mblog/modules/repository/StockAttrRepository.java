/**  
* @Title: StockAttrRepository.java 
* @Description:
* @Copyright: Copyright (c) 2018
* @Company:http://www.sinocon.cn
* @author: Su.Yuanlin  
* @date 2019年3月7日  
* @QQ 314078331  
* @Like 流水落花春去也，天上人间
*/  
package com.mtons.mblog.modules.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.mtons.mblog.modules.entity.StockAttr;

/**  
* @Title: StockAttrRepository 
* @Description:
* @author:Su.Yuanlin
* @date 2019年3月7日  
*/
public interface StockAttrRepository extends JpaRepository<StockAttr, String>, JpaSpecificationExecutor<StockAttr>{

	/**  
	 * @Title: findByUpdateTime
	 * @Description:
	 * @param today  
	 * @return 
	
	 */  
	List<StockAttr> findByUpdatetime(Date updatetime);

}
