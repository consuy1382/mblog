/**  
* @Title: HolidayService.java 
* @Description:
* @Copyright: Copyright (c) 2018
* @Company:http://www.sinocon.cn
* @author: Su.Yuanlin  
* @date 2019年3月18日  
* @QQ 314078331  
* @Like 流水落花春去也，天上人间
*/
package com.mtons.mblog.modules.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.mtons.mblog.modules.entity.Holiday;

/**
 * @Title: HolidayService
 * @Description:
 * @author:Su.Yuanlin
 * @date 2019年3月18日
 */
public interface HolidayService {
	void saveOrUpdate(Holiday holiday);

	void delete(List<String> dataCode);

	Holiday findHolidayById(String dataCode);

	/**  
	 * @Title: paging
	 * @Description:
	 * @param pageable
	 * @param i
	 * @param object
	 * @param order
	 * @return  
	
	 */  
	Page<Holiday> paging(Pageable pageable, int i, Object object, String order);
}
