/**  
* @Title: TaobaoService.java 
* @Description:
* @Copyright: Copyright (c) 2018
* @Company:http://www.sinocon.cn
* @author: Su.Yuanlin  
* @date 2019年4月11日  
* @QQ 314078331  
* @Like 流水落花春去也，天上人间
*/
package com.mtons.mblog.modules.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.mtons.mblog.modules.data.AnalysisVO;
import com.mtons.mblog.modules.data.SalesVo;
import com.mtons.mblog.modules.entity.Product;
import com.mtons.mblog.modules.entity.Sales;

/**
 * @Title: TaobaoService
 * @Description:
 * @author:Su.Yuanlin
 * @date 2019年4月11日
 */
public interface TaobaoService {
	/**
	 * 保存商品
	 * 
	 * @Title: saveOrUpdateProduct
	 * @Description:
	 * @param p
	 * @return
	 */
	int saveOrUpdateProduct(Product p);

	/**
	 * 批量保存商品
	 * 
	 * @Title: addProduct
	 * @Description:
	 * @param listProduct
	 * @return
	 */
	int addProduct(List<Product> listProduct);

	/**
	 * 删除商品byID
	 * 
	 * @Title: delProduct
	 * @Description:
	 * @param id
	 * @return
	 */
	int delProduct(String id);

	/**
	 * 获取所有商品，查询有缓存（有新数据会自动更新）
	 * 
	 * @Title: getProductAll
	 * @Description:
	 * @return
	 */
	List<Product> getProductAll();

	/**
	 * 模糊查询商品
	 * 
	 * @Title: getProductByProductName
	 * @Description:
	 * @param productName
	 * @return
	 */
	Page<Product> getProductByProductName(Pageable pageable, String productName);

	/**
	 * 精准查询商品
	 * 
	 * @Title: getProductById
	 * @Description:
	 * @param id
	 * @return
	 */
	Product getProductById(String id);

	int saveOrUpdateSales(Sales sales);

	int addSales(List<Sales> list);

	Page<SalesVo> getSalesByTime(Pageable pageable, String time, String end, String productId, String kw,
			String isNatural);

	/**
	 * @Title: getSalesById
	 * @Description:
	 * @param id
	 * @return
	 * 
	 */
	Sales getSalesById(String id);

	Sales getSalesByTaobaoOrder(String order);

	/**
	 * 
	 * @Title: checkTaobaoOrderExist
	 * @Description:
	 * @param order
	 * @return
	 */
	boolean checkTaobaoOrderExist(String order);

	AnalysisVO getAnalysisData(String productId, String isNatural, String begin, String end, String status, String kw);
}
