/**  
* @Title: StockService.java 
* @Description:
* @Copyright: Copyright (c) 2018
* @Company:http://www.sinocon.cn
* @author: Su.Yuanlin  
* @date 2019年3月12日  
* @QQ 314078331  
* @Like 流水落花春去也，天上人间
*/
package com.mtons.mblog.modules.service;

import java.util.Date;
import java.util.List;
import java.util.Set;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.mtons.mblog.modules.data.StockVo;
import com.mtons.mblog.modules.entity.Stock;
import com.mtons.mblog.modules.entity.StockAttr;
import com.mtons.mblog.modules.entity.StockHistory;

/**
 * @Title: StockService
 * @Description:
 * @author:Su.Yuanlin
 * @date 2019年3月12日
 */

public interface StockService {

	/**
	 * 分页查询所有股票
	 * 
	 * @param pageable
	 * @param channelId
	 *            分组Id
	 * @param ord
	 *            排序
	 */
	
	Page<StockVo> paging(Pageable pageable, int channelId, Set<Integer> excludeChannelIds, String ord);

	/**
	 * 查询排行榜
	 * @Title: findHotStock
	 * @Description:
	 * @param size
	 * @return
	 * 
	 */
	
	List<StockVo> findHotStock(Integer maxResults);

	/**
	 * 更新股票热度方法
	 * 
	 * @param p
	 */
	
	void update(StockVo p);
	
	/**  
	 * @Title: saveUpdatedStock
	 * @Description:
	 * @param t  
	
	 */  
	void saveUpdatedStock(StockAttr t);

	/**  
	 * @Title: listUpdatedStock
	 * @Description:
	 * @param hashMap
	 * @return  
	
	 */  
	List<StockAttr> listStockAttrByUpdateTime(Date updateTime);

	/**  
	 * @Title: save
	 * @Description:
	 * @param t  
	
	 */  
	void save(Stock t);

	/**  
	 * @Title: findKeyWord
	 * @Description:
	 * @param term
	 * @return  
	
	 */  
	List<Stock> findKeyWord(String term);

	/**  
	 * @Title: saveHistoryPrice
	 * @Description:
	 * @param sh
	 * @return  
	
	 */  
	int saveHistoryPrice(List<StockHistory> entitys);

	/**  
	 * @Title: findStockHistoryById
	 * @Description:
	 * @param stockcode
	 * @param year
	 * @return  
	
	 */  
	List<StockHistory> findStockHistoryById(String stockcode, String year,Integer quarter,Integer month);
	
	List<StockHistory> findStockHistroy();

}
