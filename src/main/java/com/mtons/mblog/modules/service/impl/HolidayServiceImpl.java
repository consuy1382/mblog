/**  
* @Title: HolidayServiceImpl.java 
* @Description:
* @Copyright: Copyright (c) 2018
* @Company:http://www.sinocon.cn
* @author: Su.Yuanlin  
* @date 2019年3月18日  
* @QQ 314078331  
* @Like 流水落花春去也，天上人间
*/
package com.mtons.mblog.modules.service.impl;

import java.util.List;

import org.jsoup.helper.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.mtons.mblog.modules.data.StockVo;
import com.mtons.mblog.modules.entity.Holiday;
import com.mtons.mblog.modules.entity.Stock;
import com.mtons.mblog.modules.repository.HolidayRepository;
import com.mtons.mblog.modules.service.HolidayService;

import cn.hutool.core.util.ObjectUtil;
import lombok.extern.slf4j.Slf4j;

/**
 * @Title: HolidayServiceImpl
 * @Description:
 * @author:Su.Yuanlin
 * @date 2019年3月18日
 */
@Slf4j
@Service
public class HolidayServiceImpl implements HolidayService {

	@Autowired
	private HolidayRepository holidayRepository;

	/*
	 * (non-Javadoc)
	 * 
	 * <p>Title: saveOrUpdate</p>
	 * 
	 * <p>Description: </p>
	 * 
	 * @param holiday
	 * 
	 * @see
	 * com.mtons.mblog.modules.service.HolidayService#saveOrUpdate(com.mtons.
	 * mblog.modules.entity.Holiday)
	 * 
	 */
	@Override
	public void saveOrUpdate(Holiday holiday) {
		if (ObjectUtil.isNotNull(holiday)) {
			this.holidayRepository.save(holiday);
			log.info("节假日保存成功！！！");
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * <p>Title: delete</p>
	 * 
	 * <p>Description: </p>
	 * 
	 * @param dataCode
	 * 
	 * @see
	 * com.mtons.mblog.modules.service.HolidayService#delete(java.lang.String)
	 * 
	 */
	@Override
	public void delete(List<String> dataCode) {
		if (!ObjectUtil.isNotNull(dataCode)) {
			this.holidayRepository.removeByDateCodeIn(dataCode);
			log.info("删除节假日成功{0}", dataCode);
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * <p>Title: findHolidayById</p>
	 * 
	 * <p>Description: </p>
	 * 
	 * @param dataCode
	 * 
	 * @return
	 * 
	 * @see
	 * com.mtons.mblog.modules.service.HolidayService#findHolidayById(java.lang.
	 * String)
	 * 
	 */
	@Override
	public Holiday findHolidayById(String dataCode) {
		if (!StringUtil.isBlank(dataCode))
			return this.holidayRepository.findById(dataCode).get();
		return null;
	}

	
	@Override
	public Page<Holiday> paging(Pageable pageable, int i, Object object, String order) {
		Pageable p = PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(),
				new Sort(Sort.Direction.ASC, order));

		Page<Holiday> page = this.holidayRepository.findAll(p);
		return page;
	}

}
