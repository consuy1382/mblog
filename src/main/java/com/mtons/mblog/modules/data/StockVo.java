/**  
* @Title: StockVo.java 
* @Description:
* @Copyright: Copyright (c) 2018
* @Company:http://www.sinocon.cn
* @author: Su.Yuanlin  
* @date 2019年3月12日  
* @QQ 314078331  
* @Like 流水落花春去也，天上人间
*/  
package com.mtons.mblog.modules.data;

import java.io.Serializable;

import com.mtons.mblog.modules.entity.Stock;

/**  
* @Title: StockVo 
* @Description:
* @author:Su.Yuanlin
* @date 2019年3月12日  
*/
public class StockVo extends Stock implements Serializable {

	/** serialVersionUID*/ 
	private static final long serialVersionUID = 1L;
	private String percentage;
	/**
	 * @return the percentage
	 */
	public String getPercentage() {
		return percentage;
	}
	/**
	 * @param percentage the percentage to set
	 */
	public void setPercentage(String percentage) {
		this.percentage = percentage;
	}

}
