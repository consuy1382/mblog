/**  
* @Title: MusicVo.java 
* @Description:
* @Copyright: Copyright (c) 2018
* @Company:http://www.sinocon.cn
* @author: Su.Yuanlin  
* @date 2019年3月25日  
* @QQ 314078331  
* @Like 流水落花春去也，天上人间
*/
package com.mtons.mblog.modules.data;

import java.io.Serializable;

/**
 * @Title: MusicVo
 * @Description:
 * @author:Su.Yuanlin
 * @date 2019年3月25日
 */
public class MusicVo implements Serializable {

	/** serialVersionUID */
	private static final long serialVersionUID = 1L;
	private String musicName;
	private String musicUrl;
	private String musicAuth;
	private String musicPic;
	private String musicId;

	/**
	 * @return the musicId
	 */
	public String getMusicId() {
		return musicId;
	}

	/**
	 * @param musicId the musicId to set
	 */
	public void setMusicId(String musicId) {
		this.musicId = musicId;
	}

	/**
	 * @return the musicName
	 */
	public String getMusicName() {
		return musicName;
	}

	/**
	 * @param musicName
	 *            the musicName to set
	 */
	public void setMusicName(String musicName) {
		this.musicName = musicName;
	}

	/**
	 * @return the musicUrl
	 */
	public String getMusicUrl() {
		return musicUrl;
	}

	/**
	 * @param musicUrl
	 *            the musicUrl to set
	 */
	public void setMusicUrl(String musicUrl) {
		this.musicUrl = musicUrl;
	}

	/**
	 * @return the musicAuth
	 */
	public String getMusicAuth() {
		return musicAuth;
	}

	/**
	 * @param musicAuth
	 *            the musicAuth to set
	 */
	public void setMusicAuth(String musicAuth) {
		this.musicAuth = musicAuth;
	}

	/**
	 * @return the musicPic
	 */
	public String getMusicPic() {
		return musicPic;
	}

	/**
	 * @param musicPic the musicPic to set
	 */
	public void setMusicPic(String musicPic) {
		this.musicPic = musicPic;
	}

}
