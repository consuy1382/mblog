/**  
* @Title: AnalysisVO.java 
* @Description:
* @Copyright: Copyright (c) 2018
* @Company:http://www.sinocon.cn
* @author: Su.Yuanlin  
* @date 2019年4月16日  
* @QQ 314078331  
* @Like 流水落花春去也，天上人间
*/
package com.mtons.mblog.modules.data;

/**
 * @Title: AnalysisVO
 * @Description:
 * @author:Su.Yuanlin
 * @date 2019年4月16日
 */
public class AnalysisVO {
	private String productId;
	private String commissionFeeCnt;// 佣金
	private String principalFeeCnt;// 本金
	private String sellNumCnt;// 卖出总数
	private String sellFeeCnt;// 卖出总金额
	private String expressFeeCnt;// 快递总花费

	/**
	 * @return the productId
	 */
	public String getProductId() {
		return productId;
	}

	/**
	 * @param productId
	 *            the productId to set
	 */
	public void setProductId(String productId) {
		this.productId = productId;
	}

	/**
	 * @return the commissionFeeCnt
	 */
	public String getCommissionFeeCnt() {
		return commissionFeeCnt;
	}

	/**
	 * @param commissionFeeCnt
	 *            the commissionFeeCnt to set
	 */
	public void setCommissionFeeCnt(String commissionFeeCnt) {
		this.commissionFeeCnt = commissionFeeCnt;
	}

	/**
	 * @return the principalFeeCnt
	 */
	public String getPrincipalFeeCnt() {
		return principalFeeCnt;
	}

	/**
	 * @param principalFeeCnt
	 *            the principalFeeCnt to set
	 */
	public void setPrincipalFeeCnt(String principalFeeCnt) {
		this.principalFeeCnt = principalFeeCnt;
	}

	/**
	 * @return the sellNumCnt
	 */
	public String getSellNumCnt() {
		return sellNumCnt;
	}

	/**
	 * @param sellNumCnt
	 *            the sellNumCnt to set
	 */
	public void setSellNumCnt(String sellNumCnt) {
		this.sellNumCnt = sellNumCnt;
	}

	/**
	 * @return the sellFeeCnt
	 */
	public String getSellFeeCnt() {
		return sellFeeCnt;
	}

	/**
	 * @param sellFeeCnt
	 *            the sellFeeCnt to set
	 */
	public void setSellFeeCnt(String sellFeeCnt) {
		this.sellFeeCnt = sellFeeCnt;
	}

	/**
	 * @return the expressFeeCnt
	 */
	public String getExpressFeeCnt() {
		return expressFeeCnt;
	}

	/**
	 * @param expressFeeCnt
	 *            the expressFeeCnt to set
	 */
	public void setExpressFeeCnt(String expressFeeCnt) {
		this.expressFeeCnt = expressFeeCnt;
	}

}
