/**  
* @Title: SalesVo.java 
* @Description:
* @Copyright: Copyright (c) 2018
* @Company:http://www.sinocon.cn
* @author: Su.Yuanlin  
* @date 2019年4月12日  
* @QQ 314078331  
* @Like 流水落花春去也，天上人间
*/  
package com.mtons.mblog.modules.data;

import java.io.Serializable;

import com.mtons.mblog.modules.entity.Product;
import com.mtons.mblog.modules.entity.Sales;

/**  
* @Title: SalesVo 
* @Description:
* @author:Su.Yuanlin
* @date 2019年4月12日  
*/
public class SalesVo extends Sales implements Serializable {

	/** serialVersionUID*/ 
	private static final long serialVersionUID = 1L;
	private Product product;
	/**
	 * @return the product
	 */
	public Product getProduct() {
		return product;
	}
	/**
	 * @param product the product to set
	 */
	public void setProduct(Product product) {
		this.product = product;
	}
	

}
