/**  
* @Title: TUpdateStock.java 
* @Description:
* @Copyright: Copyright (c) 2018
* @Company:http://www.sinocon.cn
* @author: Su.Yuanlin  
* @date 2019年1月10日  
* @QQ 314078331  
* @Like 流水落花春去也，天上人间
*/
package com.mtons.mblog.modules.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * @Title: TUpdateStock
 * @Description:
 * @author:Su.Yuanlin
 * @date 2019年1月10日
 */
@Entity
@Table(name = "mto_updatestock")
public class StockAttr implements Serializable {
	/** serialVersionUID */
	private static final long serialVersionUID = 1L;
	@Id
	private String stockcode;
	@Temporal(value = TemporalType.TIMESTAMP)
	private Date updatetime;

	/**
	 * @return the stockcode
	 */
	public String getStockcode() {
		return stockcode;
	}

	/**
	 * @param stockcode
	 *            the stockcode to set
	 */
	public void setStockcode(String stockcode) {
		this.stockcode = stockcode;
	}

	/**
	 * @return the updatetime
	 */
	public Date getUpdatetime() {
		return updatetime;
	}

	/**
	 * @param updatetime
	 *            the updatetime to set
	 */
	public void setUpdatetime(Date updatetime) {
		this.updatetime = updatetime;
	}

	/**
	 * @Title:
	 * @Description:
	 * @param stockcode
	 * @param updatetime
	 */
	public StockAttr(String stockcode, Date updatetime) {
		super();
		this.stockcode = stockcode;
		this.updatetime = updatetime;
	}

	/**
	 * @Title:
	 * @Description:
	 */
	public StockAttr() {
		super();
	}
}
