package com.mtons.mblog.modules.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.apache.lucene.analysis.cn.smart.SmartChineseAnalyzer;
import org.hibernate.search.annotations.Analyzer;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Indexed;

/**
 * 
 */

@Entity
@Table(name = "mto_stock")
@Indexed(index = "stock")
@Analyzer(impl = SmartChineseAnalyzer.class)
public class Stock implements Serializable {
	/** serialVersionUID */
	private static final long serialVersionUID = 1L;
	
	@Id
	private String stockcode;
	@Field
	@Column(name = "stockname", length = 32)
	private String stockname;
	@Column(name = "popularity")
	private Integer popularity;
	@Column(name = "costavg", length = 32)
	private String costavg;
	@Column(name = "zcw", length = 32)
	private String zcw;
	@Column(name = "ylw", length = 32)
	private String ylw;
	@Column(name = "jetondesc", length = 200)
	private String jetondesc;
	@Column(name = "investdesc", length = 200)
	private String investdesc;
	@Column(name = "popularitydesc", length = 200)
	private String popularitydesc;
	@Temporal(value = TemporalType.TIMESTAMP)
	private Date updatetime;
	@Column(name = "price", length = 10)
	private String price;

	/**
	 * @return the stockcode
	 */
	public String getStockcode() {
		return stockcode;
	}

	/**
	 * @param stockcode
	 *            the stockcode to set
	 */
	public void setStockcode(String stockcode) {
		this.stockcode = stockcode;
	}

	/**
	 * @return the stockname
	 */
	public String getStockname() {
		return stockname;
	}

	/**
	 * @param stockname
	 *            the stockname to set
	 */
	public void setStockname(String stockname) {
		this.stockname = stockname;
	}

	/**
	 * @return the popularity
	 */
	public Integer getPopularity() {
		return popularity;
	}

	/**
	 * @param popularity
	 *            the popularity to set
	 */
	public void setPopularity(Integer popularity) {
		this.popularity = popularity;
	}

	/**
	 * @return the costavg
	 */
	public String getCostavg() {
		return costavg;
	}

	/**
	 * @param costavg
	 *            the costavg to set
	 */
	public void setCostavg(String costavg) {
		this.costavg = costavg;
	}

	/**
	 * @return the zcw
	 */
	public String getZcw() {
		return zcw;
	}

	/**
	 * @param zcw
	 *            the zcw to set
	 */
	public void setZcw(String zcw) {
		this.zcw = zcw;
	}

	/**
	 * @return the ylw
	 */
	public String getYlw() {
		return ylw;
	}

	/**
	 * @param ylw
	 *            the ylw to set
	 */
	public void setYlw(String ylw) {
		this.ylw = ylw;
	}

	/**
	 * @return the jetondesc
	 */
	public String getJetondesc() {
		return jetondesc;
	}

	/**
	 * @param jetondesc
	 *            the jetondesc to set
	 */
	public void setJetondesc(String jetondesc) {
		this.jetondesc = jetondesc;
	}

	/**
	 * @return the investdesc
	 */
	public String getInvestdesc() {
		return investdesc;
	}

	/**
	 * @param investdesc
	 *            the investdesc to set
	 */
	public void setInvestdesc(String investdesc) {
		this.investdesc = investdesc;
	}

	/**
	 * @return the popularitydesc
	 */
	public String getPopularitydesc() {
		return popularitydesc;
	}

	/**
	 * @param popularitydesc
	 *            the popularitydesc to set
	 */
	public void setPopularitydesc(String popularitydesc) {
		this.popularitydesc = popularitydesc;
	}

	/**
	 * @return the updatetime
	 */
	public Date getUpdatetime() {
		return updatetime;
	}

	/**
	 * @return the price
	 */
	public String getPrice() {
		return price;
	}

	/**
	 * @param price
	 *            the price to set
	 */
	public void setPrice(String price) {
		this.price = price;
	}

	/**
	 * @param updatetime
	 *            the updatetime to set
	 */
	public void setUpdatetime(Date updatetime) {
		this.updatetime = updatetime;
	}
}
