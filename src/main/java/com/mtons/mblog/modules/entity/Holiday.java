/**  
* @Title: Holiday.java 
* @Description:
* @Copyright: Copyright (c) 2018
* @Company:http://www.sinocon.cn
* @author: Su.Yuanlin  
* @date 2019年3月18日  
* @QQ 314078331  
* @Like 流水落花春去也，天上人间
*/
package com.mtons.mblog.modules.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * @Title: Holiday
 * @Description:
 * @author:Su.Yuanlin
 * @date 2019年3月18日
 */
@Entity
@Table(name = "mto_holiday")
public class Holiday {
	@Id
	private String dateCode;
	private String year;
	private String remark;
	private Integer holidayType;
	@Temporal(TemporalType.TIMESTAMP)
	private Date createTime;

	/**
	 * @return the dateCode
	 */
	public String getDateCode() {
		return dateCode;
	}

	/**
	 * @param dateCode
	 *            the dateCode to set
	 */
	public void setDateCode(String dateCode) {
		this.dateCode = dateCode;
	}

	/**
	 * @return the year
	 */
	public String getYear() {
		return year;
	}

	/**
	 * @param year
	 *            the year to set
	 */
	public void setYear(String year) {
		this.year = year;
	}

	/**
	 * @return the remark
	 */
	public String getRemark() {
		return remark;
	}

	/**
	 * @param remark
	 *            the remark to set
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}

	/**
	 * @return the createTime
	 */
	public Date getCreateTime() {
		return createTime;
	}

	/**
	 * @param createTime
	 *            the createTime to set
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	/**
	 * @return the holidayType
	 */
	public Integer getHolidayType() {
		return holidayType;
	}

	/**
	 * @param holidayType the holidayType to set
	 */
	public void setHolidayType(Integer holidayType) {
		this.holidayType = holidayType;
	}

}
