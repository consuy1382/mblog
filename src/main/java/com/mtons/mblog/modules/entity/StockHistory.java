/**  
* @Title: StockHistory.java 
* @Description:
* @Copyright: Copyright (c) 2018
* @Company:http://www.sinocon.cn
* @author: Su.Yuanlin  
* @date 2019年3月14日  
* @QQ 314078331  
* @Like 流水落花春去也，天上人间
*/
package com.mtons.mblog.modules.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.search.annotations.Field;

/**
 * @Title: StockHistory
 * @Description:
 * @author:Su.Yuanlin
 * @date 2019年3月14日
 */
@Entity
@Table(name = "mto_stock_history",indexes = {
		@Index(name = "IK_STOCK_HISTORY_ID", columnList = "stockcode")
})
public class StockHistory implements Serializable {
	/** serialVersionUID */
	private static final long serialVersionUID = 1L;
	@Id
	@Column(name = "stockid", length = 32)
	private String stockid;

	@Column(name = "updatetime", length = 32)
	private String updatetime;

	@Column(name = "stockcode", length = 32)
	private String stockcode;

	@Field
	@Column(name = "stockname", length = 32)
	private String stockname;

	@Field
	@Lob
	@Column(columnDefinition = "text")
	private String price;

	@Column(name = "createtime", length = 32)
	private String createtime;

	@Lob
	@Column(columnDefinition = "text")
	private String updatetimeStr;
	
	private Integer quarter;
	
	@Transient
	private String year;

	/**
	 * @return the stockcode
	 */
	public String getStockcode() {
		return stockcode;
	}

	/**
	 * @param stockcode
	 *            the stockcode to set
	 */
	public void setStockcode(String stockcode) {
		this.stockcode = stockcode;
	}

	/**
	 * @return the price
	 */
	public String getPrice() {
		return price;
	}

	/**
	 * @param price
	 *            the price to set
	 */
	public void setPrice(String price) {
		this.price = price;
	}

	/**
	 * @return the stockname
	 */
	public String getStockname() {
		return stockname;
	}

	/**
	 * @return the updatetime
	 */
	public String getUpdatetime() {
		return updatetime;
	}

	/**
	 * @param updatetime
	 *            the updatetime to set
	 */
	public void setUpdatetime(String updatetime) {
		this.updatetime = updatetime;
	}

	/**
	 * @param stockname
	 *            the stockname to set
	 */
	public void setStockname(String stockname) {
		this.stockname = stockname;
	}

	/**
	 * @return the createtime
	 */
	public String getCreatetime() {
		return createtime;
	}

	/**
	 * @param createtime
	 *            the createtime to set
	 */
	public void setCreatetime(String createtime) {
		this.createtime = createtime;
	}

	/**
	 * @return the updatetimeStr
	 */
	public String getUpdatetimeStr() {
		return updatetimeStr;
	}

	/**
	 * @param updatetimeStr
	 *            the updatetimeStr to set
	 */
	public void setUpdatetimeStr(String updatetimeStr) {
		this.updatetimeStr = updatetimeStr;
	}

	/**
	 * @return the year
	 */
	public String getYear() {
		return year;
	}

	/**
	 * @param year
	 *            the year to set
	 */
	public void setYear(String year) {
		this.year = year;
	}

	/**
	 * @return the stockid
	 */
	public String getStockid() {
		return stockid;
	}

	/**
	 * @param stockid the stockid to set
	 */
	public void setStockid(String stockid) {
		this.stockid = stockid;
	}

	/**
	 * @return the quarter
	 */
	public Integer getQuarter() {
		return quarter;
	}

	/**
	 * @param quarter the quarter to set
	 */
	public void setQuarter(Integer quarter) {
		this.quarter = quarter;
	}

}
