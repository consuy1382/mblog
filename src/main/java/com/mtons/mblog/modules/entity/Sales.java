/**  
* @Title: Sales.java 
* @Description:
* @Copyright: Copyright (c) 2018
* @Company:http://www.sinocon.cn
* @author: Su.Yuanlin  
* @date 2019年4月11日  
* @QQ 314078331  
* @Like 流水落花春去也，天上人间
*/
package com.mtons.mblog.modules.entity;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.jeecgframework.poi.excel.annotation.Excel;

import com.alibaba.fastjson.annotation.JSONField;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;

/**
 * @Title: Sales
 * @Description:
 * @author:Su.Yuanlin
 * @date 2019年4月11日
 */
@Entity
@Table(name = "mto_sales")
public class Sales implements Serializable {
	/** serialVersionUID*/ 
	private static final long serialVersionUID = 1L;
	@Id
	private String salesId;// 主键
	@Column(name = "product_id")
	private String productId;//商品自动ID
	
	@Excel(name="宝贝标题")
	@Transient
	private String productName;
	
	@Excel(name="买家会员名")
	private String buyer;//购买人旺旺名称
	
	@Excel(name="买家支付宝账号")
	private String wangwangID;//购买人旺旺ID
	
	@Excel(name="宝贝总数量")
	private Integer buyCnt;//购买数量
	
	@Excel(name="买家实际支付金额")
	private float buyPrice;//商品成交价格
	
	@JSONField(format="yyyy-MM-dd")
	private Date buyTime;//商品购买日期
	
	@Excel(name="买家留言")
	private String buyMark;//购买备注
	
	@Excel(name="收货人姓名")
	private String nickName;//购买人姓名
	
	private int sex;//购买人性别
	
	@Excel(name="联系手机")
	private String phoneNumber;//购买人电话
	
	@Excel(name="订单状态")
	private String status;//订单状态
	
	@Excel(name="订单编号")
	private String taobaoOrder;//淘宝成交单号
	
	@Excel(name="收货地址")
	private String buyLocation;//购买人城市
	
	private Integer isNatural;// 是否正常购买||刷单
	private float expressFee;// 快递费
	
	@Excel(name="物流单号")
	private String expressNumber;//快递单号
	
	@Excel(name="物流公司")
	private String expressCompany;//快递公司
	
	private float commissionFee;//快递费用
	
	@Excel(name="订单付款时间")
	@Transient
	private String createTime;
	/**
	 * @return the salesId
	 */
	public String getSalesId() {
		return salesId;
	}
	/**
	 * @param salesId the salesId to set
	 */
	public void setSalesId(String salesId) {
		this.salesId = salesId;
	}
	/**
	 * @return the productId
	 */
	public String getProductId() {
		return productId;
	}
	/**
	 * @param productId the productId to set
	 */
	public void setProductId(String productId) {
		this.productId = productId;
	}
	/**
	 * @return the buyer
	 */
	public String getBuyer() {
		return buyer;
	}
	/**
	 * @param buyer the buyer to set
	 */
	public void setBuyer(String buyer) {
		this.buyer = buyer;
	}
	/**
	 * @return the wangwangID
	 */
	public String getWangwangID() {
		return wangwangID;
	}
	/**
	 * @param wangwangID the wangwangID to set
	 */
	public void setWangwangID(String wangwangID) {
		this.wangwangID = wangwangID;
	}
	/**
	 * @return the buyCnt
	 */
	public Integer getBuyCnt() {
		return buyCnt;
	}
	/**
	 * @param buyCnt the buyCnt to set
	 */
	public void setBuyCnt(Integer buyCnt) {
		this.buyCnt = buyCnt;
	}
	/**
	 * @return the buyPrice
	 */
	public float getBuyPrice() {
		return buyPrice;
	}
	/**
	 * @param buyPrice the buyPrice to set
	 */
	public void setBuyPrice(float buyPrice) {
		this.buyPrice = buyPrice;
	}
	/**
	 * @return the buyTime
	 */
	public Date getBuyTime() {
		return buyTime;
	}
	/**
	 * @param buyTime the buyTime to set
	 */
	public void setBuyTime(Date buyTime) {
		this.buyTime = buyTime;
	}
	/**
	 * @return the buyMark
	 */
	public String getBuyMark() {
		return buyMark;
	}
	/**
	 * @param buyMark the buyMark to set
	 */
	public void setBuyMark(String buyMark) {
		this.buyMark = buyMark;
	}
	/**
	 * @return the nickName
	 */
	public String getNickName() {
		return nickName;
	}
	/**
	 * @param nickName the nickName to set
	 */
	public void setNickName(String nickName) {
		this.nickName = nickName;
	}
	
	/**
	 * @return the sex
	 */
	public int getSex() {
		return sex;
	}
	/**
	 * @param sex the sex to set
	 */
	public void setSex(int sex) {
		this.sex = sex;
	}
	/**
	 * @return the phoneNumber
	 */
	public String getPhoneNumber() {
		return phoneNumber;
	}
	/**
	 * @param phoneNumber the phoneNumber to set
	 */
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	/**
	 * @return the taobaoOrder
	 */
	public String getTaobaoOrder() {
		return taobaoOrder;
	}
	/**
	 * @param taobaoOrder the taobaoOrder to set
	 */
	public void setTaobaoOrder(String taobaoOrder) {
		this.taobaoOrder = taobaoOrder;
	}
	/**
	 * @return the buyLocation
	 */
	public String getBuyLocation() {
		return buyLocation;
	}
	/**
	 * @param buyLocation the buyLocation to set
	 */
	public void setBuyLocation(String buyLocation) {
		this.buyLocation = buyLocation;
	}
	/**
	 * @return the isNatural
	 */
	public Integer getIsNatural() {
		return isNatural;
	}
	/**
	 * @param isNatural the isNatural to set
	 */
	public void setIsNatural(Integer isNatural) {
		this.isNatural = isNatural;
	}
	/**
	 * @return the expressFee
	 */
	public float getExpressFee() {
		return expressFee;
	}
	/**
	 * @param expressFee the expressFee to set
	 */
	public void setExpressFee(float expressFee) {
		this.expressFee = expressFee;
	}
	public String getExpressNumber() {
		return expressNumber;
	}
	public void setExpressNumber(String expressNumber) {
		this.expressNumber = expressNumber;
	}
	public String getExpressCompany() {
		return expressCompany;
	}
	public void setExpressCompany(String expressCompany) {
		this.expressCompany = expressCompany;
	}
	public String getCreateTime() {
		return DateUtil.format(this.getBuyTime(), "yyyy-MM-dd");
	}
	public void setCreateTime(String createTime) throws ParseException {
		if (ObjectUtil.isNotNull(createTime)) {
			this.createTime = createTime.indexOf("/") > 0 ? createTime.replace("/", "-") : createTime;
			this.buyTime = new SimpleDateFormat("yyyy-MM-dd").parse(this.createTime);
		}else{
			this.createTime = createTime;
			this.buyTime = null;
		}
		
	}
	/**
	 * @return the productName
	 */
	public String getProductName() {
		return productName;
	}
	/**
	 * @param productName the productName to set
	 */
	public void setProductName(String productName) {
		this.productName = productName;
	}
	/**
	 * @return the commissionFee
	 */
	public float getCommissionFee() {
		return commissionFee;
	}
	/**
	 * @param commissionFee the commissionFee to set
	 */
	public void setCommissionFee(float commissionFee) {
		this.commissionFee = commissionFee;
	}
}
