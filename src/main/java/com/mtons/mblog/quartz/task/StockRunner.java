/**  
* @Title: StockRunner.java 
* @Description:
* @Copyright: Copyright (c) 2018
* @Company:http://www.sinocon.cn
* @author: Su.Yuanlin  
* @date 2019年3月16日  
* @QQ 314078331  
* @Like 流水落花春去也，天上人间
*/
package com.mtons.mblog.quartz.task;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import com.mtons.mblog.modules.data.StockVo;
import com.mtons.mblog.modules.entity.StockHistory;
import com.mtons.mblog.modules.service.StockService;

/**
 * @Title: StockRunner
 * @Description:
 * @author:Su.Yuanlin
 * @date 2019年3月16日
 */

@Component
public class StockRunner implements ApplicationRunner{

	@Autowired
	private StockService stockService;

	@Value("${server.port}")
    private int port;
	
	@Override
	public void run(ApplicationArguments args) throws Exception {
//		execHistoryTask();
	}

	public void execHistoryTask() {
		List<List<StockVo>> result = new ArrayList<>();
		List<StockVo> orderList = stockService.findHotStock(9000);
		int cnt = 10;
		for (int i=0;i<9000;i=i+cnt) {
			int j = i;
			int k = i+cnt;
			result.add(orderList
					.stream()
					.filter(vo -> (vo.getPopularity() > j && vo.getPopularity() <= k))
					.collect(Collectors.toList()));
		}
		
		result.stream().forEach(ls->{
			Thread thread = new Thread(new MyThread(ls));
			System.out.println(thread.getName());
			thread.start();
		});
		System.out.println("开启线程"+result.size()+"个");
	}

	class MyThread implements Runnable {
		private List<StockVo> skList;

		/**
		 * @return the skList
		 */
		public List<StockVo> getSkList() {
			return skList;
		}

		/**
		 * @param skList the skList to set
		 */
		public void setSkList(List<StockVo> skList) {
			this.skList = skList;
		}

		/**  
		* @Title: 
		* @Description:  
		*/ 
		public MyThread(List<StockVo> list) {
			this.skList = list;
		}

		public String today(){
			SimpleDateFormat sf = new SimpleDateFormat("yyyyMMdd");
			return sf.format(new Date());
		}
		@Override
		public void run() {
			
			this.getSkList().stream().forEach(ckvo -> {
				List<StockHistory> stockHistyList = StockHistoryUtil.exec("20190409", today(), ckvo.getStockcode(),
						ckvo.getStockname());
				stockService.saveHistoryPrice(stockHistyList);
				System.out.println("update "+ckvo.getStockcode()+" :"+stockHistyList.size()+" success");
			});
		}
	}
}
