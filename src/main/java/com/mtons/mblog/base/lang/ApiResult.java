/**  
* @Title: ApiResult.java 
* @Description:
* @Copyright: Copyright (c) 2018
* @Company:http://www.sinocon.cn
* @author: Su.Yuanlin  
* @date 2019年3月18日  
* @QQ 314078331  
* @Like 流水落花春去也，天上人间
*/
package com.mtons.mblog.base.lang;

/**
 * @Title: ApiResult
 * @Description:
 * @author:Su.Yuanlin
 * @date 2019年3月18日
 */
public class ApiResult<T> {
	private int code;
	private String message;
	private T data;

	private ApiResult setResult(int code, String message, T data) {
		this.code = code;
		this.message = message;
		this.data = data;
		return this;
	}

	public ApiResult success() {
		return setResult(200, "SUCCESS", null);
	}

	public ApiResult success(T data) {
		return setResult(200, "SUCCESS", data);
	}

	public ApiResult fail(T errorCode) {
		return setResult(400, "未知错误", errorCode);
	}

	public ApiResult fail(T errorCode, String message) {
		return setResult(400, message, errorCode);
	}

	public ApiResult fail(T errorCode, String message, int code) {
		return setResult(code, message, errorCode);
	}

	public ApiResult error(String message, int code) {
		return setResult(code, message, null);
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}
}
