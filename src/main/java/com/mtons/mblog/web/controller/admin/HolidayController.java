/**  
* @Title: HolidayController.java 
* @Description:
* @Copyright: Copyright (c) 2018
* @Company:http://www.sinocon.cn
* @author: Su.Yuanlin  
* @date 2019年3月18日  
* @QQ 314078331  
* @Like 流水落花春去也，天上人间
*/
package com.mtons.mblog.web.controller.admin;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mtons.mblog.base.lang.Result;
import com.mtons.mblog.modules.data.TableDataInfo;
import com.mtons.mblog.modules.entity.Holiday;
import com.mtons.mblog.modules.service.HolidayService;
import com.mtons.mblog.web.controller.BaseController;

/**
 * @Title: HolidayController
 * @Description:
 * @author:Su.Yuanlin
 * @date 2019年3月18日
 */
@Controller("adminHolidayController")
@RequestMapping("/admin/holiday")
public class HolidayController extends BaseController {

	@Autowired
	private HolidayService holidayService;

	@RequestMapping("/index")
	public String index(ModelMap model) {
		return "/admin/holiday/view";
	}
	
	@RequestMapping("/edit")
	public String edit(ModelMap model) {
		return "/admin/holiday/editing";
	}

	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public String update(Holiday holiday) {
		if (holiday != null) {
			this.holidayService.saveOrUpdate(holiday);
		}
		return "redirect:/admin/holiday/index";
	}

	@RequestMapping(value = "/list")
	public @ResponseBody TableDataInfo onload(HttpServletRequest request) {
		String order = ServletRequestUtils.getStringParameter(request, "ord", "year");
		Pageable pageable = wrapPageable();
		Page<Holiday> page = holidayService.paging(pageable, 0, null, order);

		return getDataTable(page);

	}

	@RequestMapping("/delete")
	@ResponseBody
	public Result delete(@RequestParam("id") List<String> id) {
		Result data = Result.failure("操作失败");
		if (id != null) {
			try {
				this.holidayService.delete(id);
				data = Result.success();
			} catch (Exception e) {
				data = Result.failure(e.getMessage());
			}
		}
		return data;
	}
}
