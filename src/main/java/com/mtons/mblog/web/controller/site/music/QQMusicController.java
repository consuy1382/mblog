/**  
* @Title: QQMusicController.java 
* @Description:
* @Copyright: Copyright (c) 2018
* @Company:http://www.sinocon.cn
* @author: Su.Yuanlin  
* @date 2019年3月25日  
* @QQ 314078331  
* @Like 流水落花春去也，天上人间
*/
package com.mtons.mblog.web.controller.site.music;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.mtons.mblog.modules.data.MusicVo;
import com.mtons.mblog.web.controller.BaseController;
import com.mtons.mblog.web.controller.site.Views;

/**
 * @Title: QQMusicController
 * @Description:
 * @author:Su.Yuanlin
 * @date 2019年3月25日
 */
@Controller
@RequestMapping("/music")
public class QQMusicController extends BaseController {

	@RequestMapping("/index")
	public String channel(ModelMap model, MusicVo m, HttpServletRequest request) {
		MusicVo music = MusicUtil.getMusicLineAddress(m);
		model.put("music", music);
		return view(Views.MUSIC_VIEW);
	}
}
