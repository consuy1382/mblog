/**  
* @Title: StockController.java 
* @Description:
* @Copyright: Copyright (c) 2018
* @Company:http://www.sinocon.cn
* @author: Su.Yuanlin  
* @date 2019年3月8日  
* @QQ 314078331  
* @Like 流水落花春去也，天上人间
*/
package com.mtons.mblog.web.controller.site.stock;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mtons.mblog.base.lang.Consts;
import com.mtons.mblog.modules.data.StockVo;
import com.mtons.mblog.modules.data.TableDataInfo;
import com.mtons.mblog.modules.entity.StockHistory;
import com.mtons.mblog.modules.service.StockService;
import com.mtons.mblog.web.controller.BaseController;
import com.mtons.mblog.web.controller.site.Views;

import cn.hutool.core.date.DateUtil;

/**
 * @Title: StockController
 * @Description:
 * @author:Su.Yuanlin
 * @date 2019年3月8日
 */
@Controller
@RequestMapping("/stock")
public class StockController extends BaseController {

	@Autowired
	private StockService stockService;

	@RequestMapping("/index")
	public String channel(ModelMap model, HttpServletRequest request) {
		return view(Views.STOCK_VIEW);
	}

	@RequestMapping("/view/{stockcode}")
	public String analysis(ModelMap model, @PathVariable String stockcode, HttpServletRequest request) {
		String stockname = ServletRequestUtils.getStringParameter(request, "stockname", stockcode);
		model.put("stockcode", stockcode);
		model.put("stockname", stockname);
		// 下拉框年份，当前年和过去3年
		List<Integer> years = new ArrayList<>();
		List<Integer> month = new ArrayList<>();
		int currentYear = DateUtil.thisYear();
		years.add(currentYear);
		for (int i = 1; i < 3; i++) {
			years.add(currentYear - i);
		}
		for (int i=1;i<=12;i++) {
			month.add(i);
		}
		model.put("year", years);
		model.put("month", month);
		return view(Views.STOCK_ANALYSIS);
	}

	@RequestMapping("/analysis/{stockcode}")
	public @ResponseBody StockHistory analysisData(@PathVariable String stockcode, HttpServletRequest request) {
		String year = ServletRequestUtils.getStringParameter(request, "year", DateUtil.thisYear() + "");
		Integer quarter = ServletRequestUtils.getIntParameter(request, "quarter", 0);
		Integer month = ServletRequestUtils.getIntParameter(request, "month", 0);
		
		StringBuffer priceBuffer = new StringBuffer();
		StringBuffer dateBuffer = new StringBuffer();
		
		List<StockHistory> skList = this.stockService.findStockHistoryById(stockcode, year, quarter,month);
		
		skList.stream().forEach(sk->{
			priceBuffer.append(sk.getPrice()).append(",");
			dateBuffer.append(sk.getCreatetime()).append(",");
		});;
		
		StockHistory sk = null;
		if (!skList.isEmpty()) {
			sk = skList.get(0);
			sk.setPrice(priceBuffer.toString());
			sk.setUpdatetimeStr(dateBuffer.toString());
		}
		
		return sk;
	}

	@RequestMapping("/list")
	public @ResponseBody TableDataInfo posts(HttpServletRequest request) {
		String order = ServletRequestUtils.getStringParameter(request, "ord", Consts.order.POPULARITY);
		int channelId = ServletRequestUtils.getIntParameter(request, "channelId", 0);
		Pageable pageable = wrapPageable();
		Page<StockVo> page = stockService.paging(pageable, channelId, null, order);
		return getDataTable(page);
	}
}
