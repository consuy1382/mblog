/**  
* @Title: ProductController.java 
* @Description:
* @Copyright: Copyright (c) 2018
* @Company:http://www.sinocon.cn
* @author: Su.Yuanlin  
* @date 2019年4月16日  
* @QQ 314078331  
* @Like 流水落花春去也，天上人间
*/
package com.mtons.mblog.web.controller.admin;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mtons.mblog.modules.data.TableDataInfo;
import com.mtons.mblog.modules.entity.Product;
import com.mtons.mblog.modules.service.TaobaoService;
import com.mtons.mblog.web.controller.BaseController;

/**
 * @Title: ProductController
 * @Description:
 * @author:Su.Yuanlin
 * @date 2019年4月16日
 */
@Controller("productController")
@RequestMapping("/admin/taobao")
public class ProductController extends BaseController {
	@Autowired
	private TaobaoService taobaoService;

	@RequestMapping("/product/list")
	public String product(ModelMap model) {
		return "/admin/taobao/product-list";
	}

	@RequestMapping("/product/add")
	public String addProduct(ModelMap model) {
		return "/admin/taobao/product";
	}

	@RequestMapping("/productlist")
	public @ResponseBody TableDataInfo productList(HttpServletRequest request) {
		String productName = ServletRequestUtils.getStringParameter(request, "productName", "");

		Pageable pageable = wrapPageable();
		Page<Product> page = this.taobaoService.getProductByProductName(pageable, productName);

		return getDataTable(page);
	}

	@RequestMapping("/product/update")
	public String productUpdate(Product view) {
		if (view != null) {
			this.taobaoService.saveOrUpdateProduct(view);
		}
		return "redirect:/admin/taobao/product/list";
	}

	@RequestMapping("/product/update/{id}")
	public String productEdit(ModelMap model, @PathVariable String id) {
		Product product = this.taobaoService.getProductById(id);
		model.put("view", product);
		return "/admin/taobao/product-add";
	}
}
