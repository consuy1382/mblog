/**  
* @Title: HolidayController.java 
* @Description:
* @Copyright: Copyright (c) 2018
* @Company:http://www.sinocon.cn
* @author: Su.Yuanlin  
* @date 2019年3月18日  
* @QQ 314078331  
* @Like 流水落花春去也，天上人间
*/
package com.mtons.mblog.web.controller.api;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mtons.mblog.base.lang.ApiResult;
import com.mtons.mblog.modules.entity.Holiday;
import com.mtons.mblog.modules.service.HolidayService;
import com.mtons.mblog.web.controller.BaseController;

/**
 * @Title: HolidayController
 * @Description:
 * @author:Su.Yuanlin
 * @date 2019年3月18日
 */
@Controller
@RequestMapping("/api")
public class HolidayController extends BaseController {
	@Autowired
	private HolidayService holidayService;

	@RequestMapping("/holiday/{datecode}")
	public @ResponseBody ApiResult<Holiday> holiday(@PathVariable String datecode, HttpServletRequest request) {
		Holiday holiday = holidayService.findHolidayById(datecode);
		ApiResult<Holiday> result = new ApiResult<>();
		result.setData(holiday);
		return result;
	}
}
