/**  
* @Title: MusicUtil.java 
* @Description:
* @Copyright: Copyright (c) 2018
* @Company:http://www.sinocon.cn
* @author: Su.Yuanlin  
* @date 2019年3月25日  
* @QQ 314078331  
* @Like 流水落花春去也，天上人间
*/
package com.mtons.mblog.web.controller.site.music;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import com.alibaba.druid.support.json.JSONParser;
import com.mtons.mblog.modules.data.MusicVo;

import cn.hutool.core.util.StrUtil;
import cn.hutool.http.HttpUtil;
import cn.hutool.json.JSON;
import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONUtil;

/**
 * @Title: MusicUtil
 * @Description:
 * @author:Su.Yuanlin
 * @date 2019年3月25日
 */
public class MusicUtil {
	public static final String __LIST = "list";
	public static final String __SONG = "song";
	public static final String __TAB = "tab";
	public static final String __SINGER = "singer";
	public static final String __NAME = "name";
	public static final String __SONGNAME = "songname";
	public static final String __SONGMID = "songmid";
	public static final String __MP3_L = "mp3_l";
	public static final String __MID = "mid";
	public static final String __ALBUMMID = "albummid";

	public static final String QUERY_MUSIC_URL = "https://c.y.qq.com/soso/fcgi-bin/client_search_cp?&t=0&aggr=1&cr=1&catZhida=1&lossless=0&flag_qc=0&p=1&n=20&w=";
	public static final String DOWNURL = "http://www.douqq.com/qqmusic/qqapi.php";

	public static String getSeachData(String keyWord) {
		String url = QUERY_MUSIC_URL + keyWord;
		String resultStr = HttpUtil.get(url);
		int begin = resultStr.indexOf(__SONG) + 6;
		int end = resultStr.indexOf(__TAB) - 2;
		return resultStr.substring(begin, end);
	}

	public static Map<Integer, String> getMusicDic(JSONArray jsonArrayData) {
		Map<Integer, String> sonmidMap = new HashMap<Integer, String>();
		// 将音乐放入map，提供下载选择
		for (int i = 0; i < jsonArrayData.size(); i++) {
			JSON jsonObject = JSONUtil.parse(jsonArrayData.get(i)); // 获取JSON
																	// map格式数据，这里包含了音乐的所有信息集合
			Object byPath = jsonObject.getByPath("albummid");
			JSONArray jsonArray = new JSONArray(jsonObject.getByPath(__SINGER));
			Object songName = JSONUtil.parse(jsonArray.get(0)).getByPath(__NAME);
			System.out.println(i + "---------" + jsonObject.getByPath(__SONGNAME) + "------" + songName.toString() + ","
					+ byPath.toString());
			sonmidMap.put(i, jsonObject.getByPath(__SONGMID).toString());
		}
		return sonmidMap;
	}

	static String musicPic = "https://y.gtimg.cn/music/photo_new/T002R300x300M000{}.jpg?max_age=2592000";

	public static List<MusicVo> getMusicVo(JSONArray jsonArrayData) {
		List<MusicVo> result = new ArrayList<>();
		for (int i = 0; i < jsonArrayData.size(); i++) {
			JSON jsonObject = JSONUtil.parse(jsonArrayData.get(i));// 获取JSON——map格式数据，这里包含了音乐的所有信息集合

			JSONArray jsonArray = new JSONArray(jsonObject.getByPath(__SINGER));

			String albummid = jsonObject.getByPath(__ALBUMMID) + "";
			String musicAuth = JSONUtil.parse(jsonArray.get(0)).getByPath(__NAME) + "";
			String musicName = jsonObject.getByPath(__SONGNAME) + "";
			String musicId = jsonObject.getByPath(__SONGMID) + "";

			MusicVo mo = new MusicVo();
			mo.setMusicId(musicId);
			mo.setMusicName(musicName);
			mo.setMusicPic(StrUtil.format(musicPic, albummid));
			mo.setMusicAuth(musicAuth);

			result.add(mo);
		}
		return result;
	}

	public static String getDownLoadUrl(Map<Integer, String> musicDicMap, int order) {
		String post = HttpUtil.post(DOWNURL, "mid=" + musicDicMap.get(order).toString());
		JSONParser jsp = new JSONParser(post);
		JSON parse = JSONUtil.parse(jsp.parse());
		return parse.getByPath(__MP3_L).toString();
	}
	
	public static MusicVo getMusicLineAddress(MusicVo m){
		String post = HttpUtil.post(DOWNURL, "mid=" + m.getMusicId());
		JSONParser jsp = new JSONParser(post);
		JSON parse = JSONUtil.parse(jsp.parse());
		String musicUrl = parse.getByPath(__MP3_L).toString();
		m.setMusicUrl(musicUrl);
		return m;
	}

	public static Page<MusicVo> getMusicVo(String kw) {
		String seachData = getSeachData(kw);
		JSONArray musicJsonArray = getMusicJsonArray(seachData);
		List<MusicVo> musicVo = getMusicVo(musicJsonArray);
		return new PageImpl<>(musicVo);
	}

	private static JSONArray getMusicJsonArray(String jsonData) {
		JSON objectJsonData = JSONUtil.parse(jsonData);
		Object listJsonData = objectJsonData.getByPath(__LIST);
		JSON listJson = JSONUtil.parse(listJsonData);
		return new JSONArray(listJson);
	}

	public static void main(String[] args) {
		System.out.print("请输入歌曲名称：");
		Scanner scanner = new Scanner(System.in);
		String _keyWord = scanner.nextLine();

		String jsonData = getSeachData(_keyWord);

		// System.out.println(jsonData);

		JSON objectJsonData = JSONUtil.parse(jsonData);

		Object listJsonData = objectJsonData.getByPath(__LIST);
		JSON listJson = JSONUtil.parse(listJsonData);
		JSONArray jsonArrayData = new JSONArray(listJson);

		Map<Integer, String> sonmidMap = getMusicDic(jsonArrayData);

		System.out.print("请输入歌曲编号：");
		int nextInt = scanner.nextInt();
		scanner.close();
		String downLoadUrl = getDownLoadUrl(sonmidMap, nextInt);
		System.out.println("请用浏览器打开，如果是H5播放页面，则点击播放页面省略号中的下载按钮即可");
		System.out.println(downLoadUrl);
	}
}
