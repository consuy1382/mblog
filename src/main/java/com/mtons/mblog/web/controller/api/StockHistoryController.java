/**  
* @Title: StockHistoryController.java 
* @Description:
* @Copyright: Copyright (c) 2018
* @Company:http://www.sinocon.cn
* @author: Su.Yuanlin  
* @date 2019年3月20日  
* @QQ 314078331  
* @Like 流水落花春去也，天上人间
*/
package com.mtons.mblog.web.controller.api;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mtons.mblog.base.lang.ApiResult;
import com.mtons.mblog.modules.entity.StockHistory;
import com.mtons.mblog.modules.service.StockService;
import com.mtons.mblog.quartz.task.StockHistoryUtil;

/**
 * @Title: StockHistoryController
 * @Description:
 * @author:Su.Yuanlin
 * @date 2019年3月20日
 */
@Controller
@RequestMapping("/api")
public class StockHistoryController {

	@Autowired
	private StockService stockService;

	@RequestMapping("/history/{stockcode}")
	public @ResponseBody ApiResult<Object> holiday(@PathVariable String stockcode, HttpServletRequest request) {
		String begin = ServletRequestUtils.getStringParameter(request, "begin", "20180101");
		SimpleDateFormat sf = new SimpleDateFormat("yyyyMMdd");
		String end = ServletRequestUtils.getStringParameter(request, "end", sf.format(new Date()));
		String stockName = ServletRequestUtils.getStringParameter(request, "stockname", stockcode);
		List<StockHistory> stockHistyList = StockHistoryUtil.exec(begin, end, stockcode, stockName);
		stockService.saveHistoryPrice(stockHistyList);
		return new ApiResult<>();
	}
}
