/*
+--------------------------------------------------------------------------
|   Mblog [#RELEASE_VERSION#]
|   ========================================
|   Copyright (c) 2014, 2015 mtons. All Rights Reserved
|   http://www.mtons.com
|
+---------------------------------------------------------------------------
*/
package com.mtons.mblog.web.controller.site;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.mtons.mblog.modules.data.MusicVo;
import com.mtons.mblog.modules.data.PostVO;
import com.mtons.mblog.modules.data.StockVo;
import com.mtons.mblog.modules.service.PostSearchService;
import com.mtons.mblog.web.controller.BaseController;
import com.mtons.mblog.web.controller.site.music.MusicUtil;

/**
 * 文章搜索
 * 
 * @author langhsu
 *
 */
@Controller
public class SearchController extends BaseController {
	@Autowired
	private PostSearchService postSearchService;

	@RequestMapping("/search")
	public String search(String kw, ModelMap model) {
		Pageable pageable = wrapPageable();
		try {
			if (StringUtils.isNotEmpty(kw)) {

				Page<StockVo> searchStock = postSearchService.searchStock(pageable, kw);
				Page<PostVO> page = postSearchService.search(pageable, kw);
				if (searchStock.getContent().isEmpty()) {
					Page<MusicVo> musicVo = MusicUtil.getMusicVo(kw);
					model.put("music", musicVo);
				}
				model.put("stocks", searchStock);
				model.put("results", page);
				
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		model.put("kw", kw);
		return view(Views.SEARCH);
	}

}
