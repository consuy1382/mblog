/**
 * 
 */
define(function(require, exports, module) {
	J = jQuery;

	var Options = {
		init : function (options) {
	       	this.options = $.extend({}, this.defaults, options);
	    },
	
	    defaults: {
			stockUrl : '',
        	size :6,
            // callback
            onLoadStock : function () {}
        },
        
        onLoad : function(stockcode,year){
        	var opts = this.options;
        	J.ajax({
        		url: opts.hotUrl,
				data: {size : opts.size},
				cache : true,
				success : function (ret) {
					if(ret && ret.length > 0){
						opts.onLoadStock.call(ret);
					}
				}
        	})
        }
	};
	
	exports.init = function (opts) {
		Options.init(opts);
		Options.onLoad();
	}
});