/**
 * 
 */
(function($) {
	$.extend({
		table : {
			_option : {},
			_params : {},
			init : function(options) {
				$.table._option = options;
				$.table._params = options.queryParams||'';
				_striped = options.striped || false;
				$('#bootstrap-table').bootstrapTable({
					url: options.url,                                   // 请求后台的URL（*）
                    contentType: "application/x-www-form-urlencoded",   // 编码类型
                    method: 'post',                                     // 请求方式（*）
                    cache: false,                                       // 是否使用缓存
                    striped: _striped,                                  // 是否显示行间隔色
                    sortable: true,                                     // 是否启用排序
                    sortStable: true,   
                    pagination: true,   								// 是否显示分页（*）
                    pageNumber: 1,                                      // 初始化加载第一页，默认第一页
                    pageSize: 10,                                       // 每页的记录行数（*） 
                    pageList: [10, 25, 50],                             // 可供选择的每页的行数（*）
                    sidePagination: "server",
                    search: false,           							// 是否显示搜索框功能
                    showSearch: options.showSearch||false,   			// 是否显示检索信息
                    showRefresh: options.showRefresh||false, 			// 是否显示刷新按钮
        			showColumns: options.showColumns||false, 			// 是否显示隐藏某列下拉框
        			showToggle: options.showToggle||false,   			// 是否显示详细视图和列表视图的切换按钮
        			showExport: options.showExport||false,   			// 是否支持导出文件
                    queryParams: $.table.queryParams,                       // 传递参数（*）
                    columns: options.columns,                           // 显示列信息（*）
                    responseHandler: $.table.responseHandler            // 回调函数
				})
			},
			// 查询条件
            queryParams: function(params) {
            	return {
        			// 传递参数查询参数
        			pageSize:       params.limit,
        			pageNo:        params.offset / params.limit + 1,
        			searchValue:    params.search,
        			orderByColumn:  params.sort,
        			isAsc:          params.order
        		}; 
            },
            // 请求获取数据后处理回调函数
            responseHandler: function(res) {
                if (res.code == 0) {
                    return { rows: res.rows, total: res.total };
                } else {
                	console.log(res.msg)
                	return { rows: [], total: 0 };
                }
            },
         // 搜索-默认第一个form
            search: function(formId) {
            	var currentId = formId|| $('form').attr('id');
    		    var params = $("#bootstrap-table").bootstrapTable('getOptions');
    		    params.queryParams = function(params) {
    		        var search = {};
    		        $.each($("#" + currentId).serializeArray(), function(i, field) {
    		            search[field.name] = field.value;
    		        });
    		        search.pageSize = params.limit;
    		        search.pageNum = params.offset / params.limit + 1;
    		        search.searchValue = params.search;
    		        search.orderByColumn = params.sort;
    		        search.isAsc = params.order;
    		        return search;
    		    }
    		    $("#bootstrap-table").bootstrapTable('refresh', params);
    		}
		},
		// 弹出层封装处理
		modal: {
			// 显示图标
    		icon: function(type) {
            	var icon = "";
        	    if (type == modal_status.WARNING) {
        	        icon = 0;
        	    } else if (type == modal_status.SUCCESS) {
        	        icon = 1;
        	    } else if (type == modal_status.FAIL) {
        	        icon = 2;
        	    } else {
        	        icon = 3;
        	    }
        	    return icon;
            },
         // 弹出层指定宽度
            open: function (url,title, width, height) {
                width = width||800;
                height = height||($(window).height() - 50);
                title = title||'';
            	layer.open({
            		type: 2,
            		area: [width + 'px', height + 'px'],
            		fix: false,
            		//不固定
            		maxmin: true,
            		shade: 0.3,
            		title: title||'',
            		content: url,
            	    // 弹层外区域关闭
            		shadeClose: true
            	});
            }
		},
		operate:{
			getCurrentDate : function(){
				var myDate = new Date();
				return myDate.getFullYear() +"-" + (parseInt(myDate.getMonth())+1) +"-"+ myDate.getDate();
			},
			getPreDate : function(){
				var myDate = new Date();
				var preDate = new Date(myDate.getTime() - 24*60*60*1000);
				return preDate.getFullYear() +"-" + (parseInt(preDate.getMonth())+1) +"-"+ preDate.getDate();
			},
			eshtml : function(html){
				var html_c = html.replace(/<\/?.+?>/g,"");
				return html_c.replace(/ /g,"");
			},
			preview : function(stock, title){
				if (stock.indexOf('6') == 0) {
					stock = 'sh'+stock;
				}else{
					stock = 'sz'+stock;
				}
				var url = 'http://market.finance.sina.com.cn/pricehis.php?symbol={0}&startdate={1}&enddate={2}';
				url = jQuery.format(url, stock, $.operate.getPreDate(), $.operate.getCurrentDate());
				$.modal.open(url,title+"成交价格占比图");
			}
		}
	})
})(jQuery);