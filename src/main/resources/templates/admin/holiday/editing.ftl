<#include "/admin/utils/ui.ftl"/>
<@layout>
<link rel='stylesheet' media='all' href='${base}/dist/css/plugins.css'/>
<script type="text/javascript" src="${base}/dist/vendors/bootstrap-tagsinput/bootstrap-tagsinput.js"></script>

<section class="content-header">
    <h1>文章编辑</h1>
    <ol class="breadcrumb">
        <li><a href="${base}/admin">首页</a></li>
        <li><a href="${base}/admin/holiday/list">节假日管理</a></li>
        <li class="active">节假日编辑</li>
    </ol>
</section>
<section class="content container-fluid">
    <div class="row">
        <form id="qForm" method="post" action="${base}/admin/holiday/update">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">节假日编辑</h3>
                    </div>
                    <div class="box-body">
                        <#include "/admin/message.ftl">
                        <div class="form-group">
                            <label>所属年份</label>
                            <input type="text" class="form-control" name="dateCode" value="${view.dateCode}" maxlength="8" placeholder="节假日代码" required/>
                        </div>
                        <div class="form-group">
                            <label>所属年份</label>
                            <input type="text" class="form-control" name="year" value="${view.year}" maxlength="64" placeholder="所属年份" required >
                        </div>
                        <div class="form-group">
                            <label>栏目</label>
                            <input type="text" class="form-control" name="remark" value="${view.remark}" maxlength="120" placeholder="简介" required >
                        </div>
                        <div class="form-group">
                            <label>节假日类型</label>
                            <select class="form-control" name="holidayType">
                                    <option value="1">工作日</option>
                                    <option value="1">周末</option>
                                    <option value="3">法定节假日</option>
                            </select>
                        </div>
                    </div>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">提交</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</section>
<script type="text/javascript">

</script>
</@layout>
