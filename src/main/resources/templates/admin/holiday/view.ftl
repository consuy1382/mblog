<#include "/admin/utils/ui.ftl"/>
<@layout>

<section class="content-header">
    <h1>节假日管理</h1>
    <ol class="breadcrumb">
        <li><a href="${base}/admin">首页</a></li>
        <li class="active">节假日管理</li>
    </ol>
</section>
<section class="content container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">节假日列表</h3>
                    <div class="box-tools">
                        <a class="btn btn-default btn-sm" href="${base}/admin/holiday/edit">添加栏目</a>
                    </div>
                </div>
                <div class="box-body">
                    <div class="table-responsive">
                        <table id="bootstrap-table" class="table table-striped"></table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
    $(function() {
            var options = {
                url: "/admin/holiday/list",
                columns: [{
					field : 'dateCode', 
					title : '日期'
				},
				{
					field : 'year',
					title : '所属年份'
				},
				{
					field : 'remark',
					title : '简介'
				},
				{
					field : 'holidayType',
					title : '节假日类型'
				}]
            };
            $.table.init(options);
            console.log(111)
    })
</script>
</@layout>