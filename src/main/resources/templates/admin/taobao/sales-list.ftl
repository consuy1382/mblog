<#include "/admin/utils/ui.ftl"/>
<@layout>

<section class="content-header">
    <h1>订单管理</h1>
    <ol class="breadcrumb">
        <li><a href="${base}/admin">首页</a></li>
        <li class="active">订单管理</li>
    </ol>
</section>
<section class="content container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">
                    	<input id="upload_btn" type="file" style="display:inline" name="file"  title="点击添加">
                    </h3>
                    <div class="box-tools">
                        <a class="btn btn-default btn-sm" href="${base}/admin/taobao/sales/add">新建</a>
                    </div>
                </div>
                <div class="box-body">
                    <form id="qForm" class="form-inline search-row">
                        <input type="hidden" name="pageNo" value="${page.number + 1}"/>
                        <div class="form-group">
                            <select class="form-control" name="productId" data-select="${productId}">
                                <option value="">查询所有商品</option>
                                <#list productList as row>
                                    <option value="${row.productId}">${row.productName}</option>
                                </#list>
                            </select>
                        </div>
                        <div class="form-group">
                            <select class="form-control" name="isNatural">
                                <option value="">请选择</option>
                                <option value="0" <#if (view.isNatural == 0)> selected </#if>>正常购物</option>
                                <option value="1" <#if (view.isNatural == 1)> selected </#if>>刷单</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <input type="text" name="kw" class="form-control" value="${kw}" placeholder="旺旺ID/名称/淘宝订单">
                        </div>
                        <div class="form-group">
                            <input type="text" name="begin" class="form-control" value="${begin}" placeholder="开始时间">
                        </div>
                        <div class="form-group">
                            <input type="text" name="end" class="form-control" value="${end}" placeholder="结束时间">
                        </div>
                        <button type="button" onclick="analysis()" class="btn btn-default">查询</button>
                    </form>
                    <div id="message">数据加载中....</div>
                    <div class="table-responsive">
                        <table id="bootstrap-table" class="table table-striped"></table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
	$(function() {
            var options = {
                url: "/admin/taobao/saleslist",
                queryParams:$("#qForm").serialize(),
                columns: [{
					field : 'taobaoOrder', 
					title : '订单编号'
				},
				{
					field : 'product.productName',
					title : '商品名称'
				},
				{
					field : 'buyer',
					title : '旺旺名称'
				},
				{
					field : 'wangwangID',
					title : '旺旺ID'
				},
				{
					field : 'nickName',
					title : '购买人<br>姓名'
				},
				{
					field : 'phoneNumber',
					title : '购买人<br>电话'
				},
				{
					title : '快递单号',
					formatter: function(value, row, index){
						var actions = [];
                        actions.push(row.expressCompany+ "<br/>" +row.expressNumber);
						return actions.join('');
					}
				},
				{
					field : 'buyTime',
					title : '购买时间'
				},
				{
					field : 'buyCnt',
					title : '购买<br>数量'
				},
				{
					field : 'buyPrice',
					title : '成交<br>价格'
				},
				{
					field : 'status',
					title : '订单状态'
				},
				{
					title: '操作',
		            align: 'center',
		            formatter: function(value, row, index) {
		            	var actions = [];
                    	actions.push('<a class="btn btn-success btn-xs" href="${base}/admin/taobao/sales/update/'+row.salesId+'"><i class="fa fa-edit"></i>编辑</a> ');
						return actions.join('');
		            }
				}
				]
            };
            $.table.init(options);
    });

	$('#upload_btn').change(function(){
        $(this).upload('${base}/admin/taobao/upload', function(json){
            layer.alert(json.message, function(){
                location.reload();
            });
        });
    });
    
    function initAnalysis(){
    	var search = {};
        $.each($("#qForm").serializeArray(), function(i, field) {
            search[field.name] = field.value;
        });
        
        $.ajax({
        	url: '${base}/admin/taobao/analysis',
        	data: search,
        	success: function(ret){
        		var actions = [];
        		actions.push('<button class="btn btn-primary" type="button">卖出数量总计 <span class="badge">'+ret.sellNumCnt+'</span></button>');
        		actions.push('<button class="btn btn-success" type="button">卖出金额总计 <span class="badge">'+parseFloat((ret.sellFeeCnt)).toFixed(2)+'￥</span></button>');
        		actions.push('<button class="btn btn-warning" type="button">快递花费总计 <span class="badge">'+ret.expressFeeCnt+'￥</span></button>');
        		actions.push('<button class="btn btn-danger" type="button">刷单佣金总计 <span class="badge">'+ret.commissionFeeCnt+'￥</span></button>');
        		actions.push('<button class="btn btn-info" type="button">成本金额总计 <span class="badge">'+ret.principalFeeCnt+'￥</span></button>');
        		actions.push('<button class="btn btn-default" type="button">本月盈利总计 <span class="badge">'+ (parseFloat((ret.sellFeeCnt)) - parseFloat(ret.principalFeeCnt) - parseFloat(ret.expressFeeCnt)).toFixed(2) +'￥</span></button>');
        		$("#message").html(actions.join(''));
        	}
        })
    }
    
    function analysis(){
        $.table.search();
        initAnalysis();
    }
    
    initAnalysis();
</script>
</@layout>
