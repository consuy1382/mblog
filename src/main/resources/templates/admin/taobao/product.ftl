<#include "/admin/utils/ui.ftl"/>
<@layout>
<link rel='stylesheet' media='all' href='${base}/dist/css/plugins.css'/>
<script type="text/javascript" src="${base}/dist/vendors/bootstrap-tagsinput/bootstrap-tagsinput.js"></script>

<section class="content-header">
    <h1>订单编辑</h1>
    <ol class="breadcrumb">
        <li><a href="${base}/admin">首页</a></li>
        <li><a href="${base}/admin/taobao/sales/list">订单管理</a></li>
        <li class="active">订单管理</li>
    </ol>
</section>
<section class="content container-fluid">
    <div class="row">
        <form id="qForm" method="post" class="form-horizontal"  action="${base}/admin/taobao/product/update" >
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">订单管理</h3>
                    </div>
                    <div class="box-body">
                        <#include "/admin/message.ftl">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">商品ID</label>
                            <div class="col-sm-6">
                            	<input type="text" class="form-control" name="productId" value="${view.productId}" maxlength="8" placeholder="商品ID" required/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">商品名称</label>
                            <div class="col-sm-6">
                            	<input type="text" class="form-control" name="productName" value="${view.productName}" maxlength="8" placeholder="商品名稱" required/>
							</div>                            	
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">商品价格</label>
                            <div class="col-sm-6">
                            	<input type="text" class="form-control" name="price" value="${view.price}" maxlength="64" placeholder="所属年份" required >
                        	</div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">进货价</label>
                            <div class="col-sm-6">
                            	<input type="text" class="form-control" name="buyingPrice" value="${view.buyingPrice}" maxlength="120" placeholder="简介" required >
                        	</div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">备注</label>
                            <div class="col-sm-6">
                            	<input type="text" class="form-control" name="remark" value="${view.remark}" maxlength="120" placeholder="简介" required >
                        	</div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">提交</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</section>
<script type="text/javascript">

</script>
</@layout>
