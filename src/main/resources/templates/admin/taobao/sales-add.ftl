<#include "/admin/utils/ui.ftl"/>
<@layout>
<link rel='stylesheet' media='all' href='${base}/dist/css/plugins.css'/>
<script type="text/javascript" src="${base}/dist/vendors/bootstrap-tagsinput/bootstrap-tagsinput.js"></script>

<section class="content-header">
    <h1>订单编辑</h1>
    <ol class="breadcrumb">
        <li><a href="${base}/admin">首页</a></li>
        <li><a href="${base}/admin/taobao/sales/list">订单管理</a></li>
        <li class="active">订单管理</li>
    </ol>
</section>
<section class="content container-fluid">
    <div class="row">
        <form id="qForm" method="post" class="form-horizontal" action="${base}/admin/taobao/sales/update" >
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">订单管理</h3>
                    </div>
                    <div class="box-body">
                        <#include "/admin/message.ftl">
                        <input type="hidden" name="salesId" value="${view.salesId}" />
                        <div class="form-group">
                            <label class="col-sm-2 control-label">淘宝订单号码:</label>
                            <div class="col-sm-6">
					            <input type="text" class="form-control" name="taobaoOrder" value="${view.taobaoOrder}" maxlength="80" placeholder="淘宝订单号码" required/>
					        </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">商品名称:</label>
                            <div class="col-sm-6">
	                            <select class="form-control" name="productId">
	                            	<#list productList as row>
	                                    <option value="${row.productId}" <#if (row.productId == view.productId)> selected </#if> >${row.productName}</option>
	                                </#list>
	                            </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">商品成交价格:</label>
                            <div class="col-sm-6">
                            	<input type="text" class="form-control" name="buyPrice" value="${view.buyPrice}" maxlength="64" placeholder="商品成交价格" required >
                        	</div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">旺旺名称:</label>
                            <div class="col-sm-6">
                            	<input type="text" class="form-control" name="buyer" value="${view.buyer}" maxlength="120" placeholder="旺旺名称" required >
                        	</div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">旺旺ID:</label>
                            <div class="col-sm-6">
                            	<input type="text" class="form-control" name="wangwangID" value="${view.wangwangID}" maxlength="120" placeholder="旺旺ID" required >
                        	</div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">购买人姓名:</label>
                            <div class="col-sm-6">
                            	<input type="text" class="form-control" name="nickName" value="${view.nickName}" maxlength="120" placeholder="购买人姓名" required >
                        	</div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">购买人电话:</label>
                            <div class="col-sm-6">
                            	<input type="text" class="form-control" name="phoneNumber" value="${view.phoneNumber}" maxlength="120" placeholder="购买人电话" required >
                        	</div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">购买时间:</label>
                            <div class="col-sm-6">
                            	<input type="text" class="form-control" name="createTime" value="${view.createTime}" maxlength="120" placeholder="购买时间"  >
                        	</div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">购买数量:</label>
                            <div class="col-sm-6">
                            	<input type="text" class="form-control" name="buyCnt" value="${view.buyCnt}" maxlength="120" placeholder="购买数量" required >
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">订单状态:</label>
                            <div class="col-sm-6">
                            	<select class="form-control" name="status">
                                    <option value="卖家已发货，等待买家确认" <#if (view.status == '卖家已发货，等待买家确认')> selected </#if>>卖家已发货，等待买家确认</option>
                                    <option value="交易成功" <#if (view.status == '交易成功')> selected </#if>>交易成功</option>
                                    <option value="买家已付款，等待卖家发货" <#if (view.status == '买家已付款，等待卖家发货')> selected </#if>>买家已付款，等待卖家发货</option>
	                            </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">购买人所在地:</label>
                            <div class="col-sm-6">
                            	<input type="text" class="form-control" name="buyLocation" value="${view.buyLocation}" maxlength="120" placeholder=购买人所在地" required >
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">物流单号:</label>
                            <div class="col-sm-6">
                            	 <input type="text" class="form-control" name="expressNumber" value="${view.expressNumber}" maxlength="120" placeholder=物流单号" >
                            </div>
                        </div>
                         <div class="form-group">
                            <label class="col-sm-2 control-label">物流费用:</label>
                            <div class="col-sm-6">
                            	 <input type="number" class="form-control" name="commissionFee" value="${view.commissionFee}" maxlength="120" placeholder=物流单号" >
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">是否正常购物:</label>
                            <div class="col-sm-6">
                            	 <select class="form-control" name="isNatural">
                                    <option value="0" <#if (view.isNatural == 0)> selected </#if>>正常购物</option>
                                    <option value="1" <#if (view.isNatural == 1)> selected </#if>>刷单</option>
	                            </select>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">提交</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</section>
<script type="text/javascript">

</script>
</@layout>
