<#include "/admin/utils/ui.ftl"/>
<@layout>

<section class="content-header">
    <h1>订单管理</h1>
    <ol class="breadcrumb">
        <li><a href="${base}/admin">首页</a></li>
        <li class="active">订单管理</li>
    </ol>
</section>
<section class="content ">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">订单列表</h3>
                    <div class="box-tools">
                        <a class="btn btn-default btn-sm" href="${base}/admin/taobao/product/add">新建</a>
                    </div>
                </div>
                <div class="box-body">
                    <form id="qForm" class="form-inline search-row">
                        <input type="hidden" name="pageNo" value="${page.number + 1}"/>
                        <div class="form-group">
                            <select class="form-control" name="channelId" data-select="${channelId}">
                                <option value="0">查询所有商品</option>
                                <#list channels as row>
                                    <option value="${row.id}">${row.name}</option>
                                </#list>
                            </select>
                        </div>
                        <div class="form-group">
                            <input type="text" name="title" class="form-control" value="${title}" placeholder="旺旺ID/名称/淘宝订单">
                        </div>
                        <button type="submit" class="btn btn-default">查询</button>
                    </form>
                    <div class="table-responsive">
                        <table id="bootstrap-table" class="table table-striped"></table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
	$(function() {
            var options = {
                url: "/admin/taobao/productlist",
                columns: [{
					field : 'productId', 
					title : '商品编号'
				},
				{
					field : 'productName',
					title : '商品名称'
				},
				{
					field : 'price',
					title : '商品价格'
				},
				{
					field : 'buyingPrice',
					title : '进货价'
				},
				{
					field : 'remark',
					title : '备注'
				},
				{
					title : '毛利润率',
					formatter : function(value, row, index){
						var p = ((parseFloat(row.price) - parseFloat(row.buyingPrice)) / parseFloat(row.price) * 100).toFixed(2);
						var actions = [];
                    	actions.push(p + "%");
						return actions.join('');
					}
				}
				]
            };
            $.table.init(options);
    })

</script>
</@layout>
