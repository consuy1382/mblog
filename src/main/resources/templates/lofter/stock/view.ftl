<#include "/lofter/inc/layout.ftl"/>

<#assign title = view.title + ' - ' + options['site_name'] />
<#assign keywords = view.keywords?default(options['site_keywords']) />
<#assign description = view.description?default(options['site_description']) />

<@layout title>
<div class="row main">
    <div class="col-xs-12 col-md-9 side-left topics-show">
        <!-- view show -->
        <div class=" panel panel-default">
            <div class="infos panel-heading">
                <h1 class="panel-title topic-title">热门排行榜(上面有搜索框)</h1>
                <div class="meta inline-block">
                    
                </div>
                <div class="clearfix"></div>
            </div>

            <div class="content-body entry-content panel-body ">
                <table id="bootstrap-table" class="table table-striped"></table>
            </div>
        </div>

        
        <!-- /view show -->
    </div>
    <div class="col-xs-12 col-md-3 side-right hidden-xs hidden-sm">
        <#include "/lofter/inc/right.ftl"/>
    </div>
</div>



<script type="text/javascript">
    $(function() {
            var options = {
                url: "/stock/list",
                columns: [{
					field : 'stockcode', 
					title : '财富密码',
					formatter: function(value, row, index) {
		            	var actions = [];
                        actions.push('<a class="btn btn-success btn-xs " target="_blank" title="查看涨幅统计" href="${base}/stock/view/'+row.stockcode+'?stockname='+row.stockname+'" >'+row.stockcode+'</a>');
						return actions.join('');
		            }
				},
				{
					field : 'stockname',
					title : '财富名称',
					formatter: function(value, row, index){
						var actions = [];
                        actions.push('<a class="btn btn-success btn-xs "  title="查看昨日交易占比" href="javascript:void(0)" onclick="$.operate.preview(\''+row.stockcode+ '\',\''+row.stockname+'\')" >'+row.stockname+'</a>');
						return actions.join('');
					}
				},
				{
					field : 'price',
					title : '当前价格'
				},
				{
					field : 'popularity',
					title : '人气排名'
				},
				{
					field : 'costavg',
					title : '平均价格'
				},
				{
					field : 'ylw',
					title : '压力位'
				},
				{
					field : 'zcw',
					title : '支撑位'
				},
				{
					field : 'investdesc',
					title : '投资理念'
				},
				{
					field : 'jetondesc',
					title : '筹码分布'
				}]
            };
            $.table.init(options);
    })

</script>
</@layout>
