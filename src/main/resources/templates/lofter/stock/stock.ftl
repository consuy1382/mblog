<#include "/lofter/inc/layout.ftl"/>

<#assign title = view.title + ' - ' + options['site_name'] />
<#assign keywords = view.keywords?default(options['site_keywords']) />
<#assign description = view.description?default(options['site_description']) />

<@layout title>
<div class="row main">
    <div class="col-xs-12 col-md-12 side-left topics-show">
    	<input type="hidden" value="${stockcode}" id="stockcode" />
    	<input type="hidden" value="${stockname}" id="stockname" />
        <!-- view show -->
        <div class=" panel panel-default">
            <div class="infos panel-heading">
                <h1 class="panel-title topic-title">
	                <select id="stockYear">
	                	<#list year as row>
	                		<option value="${row}">${row}</option>
	                	</#list>
	                </select>年
	                <select id="stockMonth">
	                	<option value="0">请选择</option>
	                	<#list month as row>
	                		<option value="${row}">${row}</option>
	                	</#list>
	                </select>月涨幅一览,年度涨幅比例<span id="billRow"></span>
				</h1>
                <div class="meta inline-block"><a href="javascript:void(1)" onclick="loadHistory()">载入历史价格(耐心等等1-10秒钟)</a></div>
                <div class="clearfix"></div>
            </div>

            <div class="content-body entry-content panel-body ">
                <div id="main" style="min-width: 600px;height:500px;"></div>
            </div>
        </div>
        <!-- /view show -->
    </div>
   
</div>


<script type="text/javascript">
	var myChart = echarts.init(document.getElementById('main'),'dark');
	
	function initStockEchart(data,xdata,yAxisName){
		var option = {
			grid: {
		        left: 30,
		        right: 50,
		        top: 50,
		        bottom: 30,
		        containLabel: true
		    },
		    tooltip: {
		        trigger: 'axis',
		        axisPointer: {
		            type: 'line',
		            lineStyle: {
		                opacity: 0
		            }
		        },
		        formatter: function(prams) {
		           return prams[0].name +"收盘价格：" + prams[0].data;
		        }
		    },
		    xAxis: {
		        type: 'category',
		        // boundaryGap: false,
		        data: xdata,
		        triggerEvent: true,
		        splitLine: {
		            show: false
		        },
		        axisLine: {
		            show: true,
		            lineStyle: {
		                width: 2,
		                color: 'rgba(255,255,255,.6)'
		            }
		        },
		        axisTick: {
		            show: false
		        },
		        axisLabel: {
		            color: '#fff',
		            fontSize: 16,
		            fontWeight: 'bold',
		            textShadowColor: '#000',
		            textShadowOffsetY: 2
		        }
	    	},
		    yAxis: {
		        name: yAxisName,
		        nameTextStyle: {
		            color: '#fff',
		            fontSize: 16,
		            textShadowColor: '#000',
		            textShadowOffsetY: 2
		        },
		        type: 'value',
		        splitLine: {
		            show: true,
		            lineStyle: {
		                color: 'rgba(255,255,255,.2)'
		            }
		        },
		        axisLine: {
		            show: true,
		            lineStyle: {
		                width: 2,
		                color: 'rgba(255,255,255,.6)'
		            }
		        },
		        axisTick: {
		            show: true
		        },
		        axisLabel: {
		            color: '#fff',
		            fontSize: 16,
		            textShadowColor: '#000',
		            textShadowOffsetY: 2
		        }
		    },
		    series: [{
		        data: data,
		        type: 'line',
		        symbol: 'circle',
		        symbolSize: 12,
		        color: '#FEC201',
		        lineStyle: {
		            color: "#03E0F2"
		        },
		        label: {
		            show: true,
		            position: 'top',
		            textStyle: {
		                color: '#FEC201',
		                fontSize: 18,
		                fontWeight: 'bold'
		            }
		        },
		        areaStyle: {
		            color: 'rgba(1,98,133,0.6)'
		        }
		    }, {
		        type: 'bar',
		        animation: false,
		        barWidth: 3,
		        hoverAnimation: false,
		        data: data,
		        tooltip: {
		            show: false
		        },
		        itemStyle: {
		            normal: {
		                color: {
		                    type: 'linear',
		                    x: 0,
		                    y: 0,
		                    x2: 0,
		                    y2: 1,
		                    colorStops: [{
		                        offset: 0,
		                        color: '#91EAF2' // 0% 处的颜色
		                    }, {
		                        offset: 1,
		                        color: '#074863' // 100% 处的颜色
		                    }],
		                    globalCoord: false // 缺省为 false
		                },
		                label: {
		                    show: false
		                }
		            }
		        }
		    }]
		};
		
		myChart.setOption(option);
	}
	seajs.use('stock', function (stock) {
		stock.init({
			stockUrl: '${base}/stock/analysis/'+$("#stockcode").val(),
			onLoadStock: function(){
				$.ajax({
					url : '${base}/stock/analysis/'+$("#stockcode").val(),
					data: {year : 2019},
					success: function(ret){ 
						initStockEchart(ret.price.split(','),ret.updatetimeStr.split(','),ret.stockname);
						calculation(ret);
					}
				});
			}
		});
	});
	
	$(document).on("change","#stockYear, #stockMonth", function(e){
    	$.ajax({
			url : '${base}/stock/analysis/'+$("#stockcode").val(),
			data: {year : $("#stockYear").val(),month:$("#stockMonth").val()},
			success: function(ret){
				if (ret) {
					initStockEchart(ret.price.split(','),ret.updatetimeStr.split(','),ret.stockname);
					calculation(ret);
				}else{
					console.log("没有数据");
				}
			}
		});
	})
	
	function calculation(ret){
		var price = ret.price.split(',');
		var billRow_txt = '0%';
		if (price.length > 1) {
			billRow_txt = (((price[price.length-2] - price[0]) / price[0] * 100).toFixed(2));
			$("#billRow").removeClass("stock-green");
			$("#billRow").removeClass("stock-red");
			if (billRow_txt < 0){
				$("#billRow").addClass("stock-green");
			}else{
				$("#billRow").addClass("stock-red");
			}
			$("#billRow").html(billRow_txt+"%");
		}
	}
	
	function loadHistory(){
		$("#main").html('<img src="${base}/dist/images/spinner.gif">');
		$.ajax({
			url : '${base}/api/history/'+$("#stockcode").val(),
			data: {stockname: $("#stockname").val()},
			success: function(ret){
				if (ret) {
					location.reload();
				}else{
					console.log("没有数据");
				}
			}
		});
	}
</script>
</@layout>
