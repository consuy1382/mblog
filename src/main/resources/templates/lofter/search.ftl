<#include "/lofter/inc/layout.ftl"/>

<@layout "搜索:" + kw>

<div class="row streams">
    <div class="col-xs-12 col-md-9 side-left">
        <div class="panel panel-default">
            <div class="panel-heading">
                <ul class="list-inline topic-filter">
                    <li class="popover-with-html">
                                                                    搜索: ${kw} 共 ${results.totalElements + stocks.totalElements} 个结果.
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>

            <div class="panel-body remove-padding-horizontal">

                <ul class="list-group row topic-list">
                	<#list music.content as row>
                		<form  action="${base}/music/index" method="post" id="music_from_${row_index}">
                			<input type="hidden" value="${row.musicId}" name="musicId">
                			<input type="hidden" value="${row.musicName}" name="musicName">
                			<input type="hidden" value="${row.musicAuth}" name="musicAuth">
                			<input type="hidden" value="${row.musicPic}" name="musicPic">
	                		<li class="list-group-item ">
	                			<a class="reply_count_area hidden-xs pull-right" href="#">
	                                <div class="count_set">
	                                    <span class="count_of_votes" title="演唱">${row.musicAuth}</span>
	                                    <span class="count_seperator">/</span>
	                                    <span class="count_of_replies" title="下载">下载</span>
	                                </div>
	                            </a>
	                            <div class="avatar pull-left">
	                                <img class="media-object img-thumbnail avatar avatar-middle" src="${row.musicPic}">
	                            </div>
	                            <div class="infos">
	                                <div class="media-heading">
	                                    <a href="javascript:void(0)" onclick="searchMusic('${row_index}')">${row.musicName}</a>
	                                </div>
	                            </div>
	                        </li>
	                    </form >
                	</#list>
                	<#list stocks.content as row>
                        <li class="list-group-item ">
                        	<input type="hidden" value="${row.stockcode}" id="stock_${row_index}" />
                        	<input type="hidden" value="${row.stockname}" id="stockname_${row_index}" />
                        	
                        	<a class="reply_count_area hidden-xs pull-right" href="javascript:void(0)" onclick="reportPrivew('stock_${row_index}','stockname_${row_index}')">
                                <div class="count_set">
                                    <span class="count_of_votes" title="年度涨幅">年度涨幅</span>
                                </div>
                            </a>
                            
                            <div class="infos">
                                <div class="media-heading">
                                   	 <a href="javascript:void(0)" onclick="priview('stock_${row_index}','stockname_${row_index}')">
	                                   	 <p>财富密码：${row.stockcode}(${row.stockname}),昨日收盘价：${row.price},压力位：${row.ylw}，支撑位：${row.zcw},人气排名：<span style="color:red">${row.popularity}</span></p>
	                                   	 <p>技术操作：${row.jetondesc}</p>
	                                   	 <p>筹码分布：${row.investdesc}</p>
                                   	 </a>
                                </div>
                            </div>
                        </li>
                    </#list>
                    
                	
                    <#list results.content as row>
                        <li class="list-group-item ">
                            <a class="reply_count_area hidden-xs pull-right" href="#">
                                <div class="count_set">
                                    <span class="count_of_votes" title="阅读数">${row.views}</span>
                                    <span class="count_seperator">/</span>
                                    <span class="count_of_replies" title="回复数">${row.comments}</span>
                                    <span class="count_seperator">/</span>
                                    <span class="count_of_visits" title="点赞数">${row.favors}</span>
                                    <span class="count_seperator">|</span>
                                    <abbr class="timeago">${timeAgo(row.created)}</abbr>
                                </div>
                            </a>
                            <div class="avatar pull-left">
                                <@utils.showAva row.author "media-object img-thumbnail avatar avatar-middle"/>
                            </div>
                            <div class="infos">
                                <div class="media-heading">
                                <#--<span class="hidden-xs label label-warning">${row.channel.name}</span>-->
                                    <a href="${base}/post/${row.id}">${row.title}</a>
                                </div>
                            </div>
                        </li>
                    </#list>

                    <#if (!results?? || results.content?size == 0) && (!stocks?? || stocks.content?size == 0)>
                        <li class="list-group-item ">
                            <div class="infos">
                                <div class="media-heading">该目录下还没有内容!</div>
                            </div>
                        </li>
                    </#if>
                </ul>
            </div>

            <div class="panel-footer text-right remove-padding-horizontal pager-footer">
                <@utils.pager request.requestURI, results, 5/>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-md-3 side-right">
        <#include "/lofter/inc/right.ftl" />
    </div>
</div>
<script>
	function priview(id,title){
		var stockCode = $.operate.eshtml($("#"+id).val());
		var stockName = $.operate.eshtml($("#"+title).val());
		$.operate.preview(stockCode,stockName);
	}
	
	function reportPrivew(id,title){
		var stockCode = $.operate.eshtml($("#"+id).val());
		var stockName = $.operate.eshtml($("#"+title).val());
		location.href = '${base}/stock/view/'+stockCode+'?stockname='+stockName;
	}
	
	function searchMusic(index){
		$("#music_from_"+index).submit();
	}
</script>
</@layout>

