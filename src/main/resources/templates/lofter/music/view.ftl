<#include "/lofter/inc/layout.ftl"/>


<link href="${base}/theme/lofter/dist/css/animate.css" rel="stylesheet"/>
<link href="${base}/theme/lofter/dist/css/music.css" rel="stylesheet"/>



<#assign title = view.title + ' - ' + options['site_name'] />
<#assign keywords = view.keywords?default(options['site_keywords']) />
<#assign description = view.description?default(options['site_description']) />
<@layout title>
<div class="row main">
    <div class="col-xs-12 col-md-12 side-left topics-show">
        <!-- view show -->
        <div class=" panel panel-default">
            <div class="infos panel-heading">
                <h1 class="panel-title topic-title">音乐:${music.musicName}</h1>
                <div class="meta inline-block"></div>
                <div class="clearfix"></div>
            </div>

            <div class="content-body entry-content panel-body " style="height: 100%;filter: blur(100px);transition:all 0.3s;background: url(${music.musicPic}) center 30% / cover no-repeat;" >
                  
            </div><div id="player" class="music-mask" style="margin-top: -22%;opacity: 0.6;"></div> 
        </div>
    </div>
</div>
<script>
var zp1 = new zplayer({
	element: document.getElementById("player"),
	musics: [{
		 title: "${music.musicName}",
         author: "${music.musicAuth}",
         url: "${music.musicUrl}",
         pic: "${music.musicPic}",
	}]
});
zp1.init();
</script>
</@layout>
